// !!! DO NOT ADD #pragma once !!! //

RGB_MATRIX_EFFECT(fade_effect)
RGB_MATRIX_EFFECT(solid_layer)
RGB_MATRIX_EFFECT(solid_layer_fade)

#ifdef RGB_MATRIX_CUSTOM_EFFECT_IMPLS

#include "quantum.h"
#include "fade.h"

// e.g: A simple effect, self-contained within a single method
// static bool fade_effect(effect_params_t* params) {
// //   RGB_MATRIX_USE_LIMITS(led_min, led_max);
//     RGB rgb = rgb_matrix_hsv_to_rgb(rgb_matrix_config.hsv);
//     rgb_matrix_set_color_all(rgb.r, rgb.g, rgb.b);
//   return false;
// }



// layers name
enum {
#   define FADE_EFFECT_LAYER(layer_name, ...) FADE_LAYER_##layer_name,
    #include "fade_effect_layers.inc"
#    undef FADE_EFFECT_LAYER
    FADE_LAYER_MAX
};


static uint8_t fade_effect_last_highest_layer = 255;

static void fade_effect_check_layer(void){

    uint8_t highest_layer = get_highest_layer(layer_state|default_layer_state);
    if(fade_effect_last_highest_layer == highest_layer)
        return;
    fade_effect_last_highest_layer = highest_layer;

    switch(highest_layer){
        default: break;
#    define FADE_EFFECT_LAYER(layer_name, red, green, blue, ...)    \
        case FADE_LAYER_##layer_name:                               \
            fade_goto(red, green, blue);                            \
            break;
        #include "fade_effect_layers.inc"
#   undef FADE_EFFECT_LAYER
    }
}

static bool fade_effect(effect_params_t* params) {
    RGB_MATRIX_USE_LIMITS(led_min, led_max);

    if (params->init) fade_init();
    fade_effect_check_layer();
    fade_rgb_scan();

    RGB rgb = rgb_matrix_hsv_to_rgb(rgb_matrix_config.hsv);

    for (uint8_t i = led_min; i < led_max; i++) {
        fade_set_color(i, rgb.r, rgb.g, rgb.b);
    }

    // uint8_t layer = get_highest_layer(layer_state);
    // for (uint8_t row = 0; row < MATRIX_ROWS; ++row) {
    //     for (uint8_t col = 0; col < MATRIX_COLS; ++col) {
    //         uint8_t index = g_led_config.matrix_co[row][col];
    //         if (index >= led_min && index < led_max && index != NO_LED &&
    //         keymap_key_to_keycode(layer, (keypos_t){col,row}) > KC_TRNS) {
    //             fade_set_color(index, rgb.r, rgb.g, rgb.b);
    //         }
    //     }
    // }

    return rgb_matrix_check_finished_leds(led_max);
}


static void solid_layer_set_color(uint8_t layer, uint8_t led_min, uint8_t led_max, uint8_t r, uint8_t g, uint8_t b){
    for (uint8_t row = 0; row < MATRIX_ROWS; ++row) {
        for (uint8_t col = 0; col < MATRIX_COLS; ++col) {
            uint8_t index = g_led_config.matrix_co[row][col];
            if (index >= led_min && index < led_max && index != NO_LED &&
                keymap_key_to_keycode(layer, (keypos_t){col,row}) > KC_TRNS) {
                fade_set_color(index, r, g, b);
            }else{
                fade_set_color(index, 0, 0, 0);
            }
        }
    }
}

static bool solid_layer(effect_params_t* params) {
    RGB_MATRIX_USE_LIMITS(led_min, led_max);

    if (params->init) rgb_matrix_set_color_all(0, 0, 0);

    uint8_t highest_layer = get_highest_layer(layer_state);
    switch(highest_layer){
        default: break;
    #define FADE_EFFECT_LAYER(layer_name, red, green, blue, ...)                        \
        case FADE_LAYER_##layer_name:                                                   \
            solid_layer_set_color(highest_layer, 0, 59, red, green, blue);   \
            break;
        #include "fade_effect_layers.inc"
    #undef FADE_EFFECT_LAYER
    }

    return rgb_matrix_check_finished_leds(led_max);
}


static bool solid_layer_fade(effect_params_t* params) {
    RGB_MATRIX_USE_LIMITS(led_min, led_max);

    if (params->init) fade_init();

    uint8_t highest_layer = get_highest_layer(layer_state);

    switch(highest_layer){
        default: break;

        #define FADE_EFFECT_LAYER(layer_name, red, green, blue, ...)          \
            case FADE_LAYER_##layer_name:                                     \
                fade_goto_advanced(highest_layer, red, green, blue);   \
                break;

            #include "fade_effect_layers.inc"
        #undef FADE_EFFECT_LAYER
    }

    fade_rgb_scan_advanced();

    return rgb_matrix_check_finished_leds(led_max);
}


#endif // RGB_MATRIX_CUSTOM_EFFECT_IMPLS
