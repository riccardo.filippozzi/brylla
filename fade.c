
#include QMK_KEYBOARD_H
#include "fade.h"

// limit time to reach fade_rgb.end
uint16_t fade_time = 0;
uint16_t fade_timer = 0;
uint8_t fade_is_running = 0;

HSV fade_default_hsv = {0, 255, 255};

typedef struct {
    uint16_t cur;     // current RGB value
    uint16_t end;     // RGB value to reach
    uint16_t step;    // RGB value to add/subtract at every step
    // uint16_t speed;   // how often to step (fade_time / fade_rgb.step)
    // uint16_t timer;   // timers
    // uint16_t time;    // time to reach end
} fade_color_t;

typedef struct {
    fade_color_t r;
    fade_color_t g;
    fade_color_t b;
} fade_rgb_t;

fade_rgb_t fade_rgb; // fader: current RGB, end RGB, step RGB
HSV fade_hsv;        // current HSV

fade_rgb_t fade_rgb_leds[RGB_MATRIX_LED_COUNT];
HSV fade_hsv_leds[RGB_MATRIX_LED_COUNT];


#define FADE_PROPORTIONS(c) ((c)*RGB_MATRIX_MAXIMUM_BRIGHTNESS)>>8

void fade_set_color(uint8_t led, uint8_t r, uint8_t g, uint8_t b){
    r = FADE_PROPORTIONS(r);
    g = FADE_PROPORTIONS(g);
    b = FADE_PROPORTIONS(b);
    rgb_matrix_set_color(led, r, g, b);
}

RGB fade_get_current_rgb(void){
    RGB rgbret = {
        r: fade_rgb.r.cur>>8,
        g: fade_rgb.g.cur>>8,
        b: fade_rgb.b.cur>>8
    };
    return rgbret;
}


/*
const uint8_t quadratic_curve[256] = {
  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   1,   1,
  1,   1,   1,   1,   2,   2,   2,   2,   2,   2,   3,   3,   3,   3,   4,   4,
  4,   4,   5,   5,   5,   5,   6,   6,   6,   7,   7,   7,   8,   8,   8,   9,
  9,   9,  10,  10,  11,  11,  11,  12,  12,  13,  13,  14,  14,  15,  15,  16,
 16,  17,  17,  18,  18,  19,  19,  20,  20,  21,  21,  22,  23,  23,  24,  24,
 25,  26,  26,  27,  28,  28,  29,  30,  30,  31,  32,  32,  33,  34,  35,  35,
 36,  37,  38,  38,  39,  40,  41,  42,  42,  43,  44,  45,  46,  47,  47,  48,
 49,  50,  51,  52,  53,  54,  55,  55,  56,  57,  58,  59,  60,  61,  62,  63,
 64,  65,  66,  67,  68,  69,  70,  71,  72,  74,  75,  76,  77,  78,  79,  80,
 81,  82,  84,  85,  86,  87,  88,  89,  91,  92,  93,  94,  95,  97,  98,  99,
100, 102, 103, 104, 105, 107, 108, 109, 111, 112, 113, 115, 116, 117, 119, 120,
121, 123, 124, 126, 127, 128, 130, 131, 133, 134, 136, 137, 139, 140, 141, 143,
144, 146, 147, 149, 151, 152, 154, 155, 157, 158, 160, 161, 163, 165, 166, 168,
170, 171, 173, 174, 176, 178, 179, 181, 183, 185, 186, 188, 190, 191, 193, 195,
197, 198, 200, 202, 204, 206, 207, 209, 211, 213, 215, 216, 218, 220, 222, 224,
226, 228, 229, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251, 253, 255
};

const uint8_t exp_curve[256] =
{
   0,   0,   0,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,
   1,   1,   1,   2,   2,   2,   2,   2,   2,   2,   2,   2,   2,   2,   2,   2,
   2,   2,   2,   2,   2,   2,   2,   2,   2,   2,   2,   3,   3,   3,   3,   3,
   3,   3,   3,   3,   3,   3,   3,   3,   3,   3,   4,   4,   4,   4,   4,   4,
   4,   4,   4,   4,   4,   4,   5,   5,   5,   5,   5,   5,   5,   5,   5,   6,
   6,   6,   6,   6,   6,   6,   6,   7,   7,   7,   7,   7,   7,   8,   8,   8,
   8,   8,   8,   9,   9,   9,   9,   9,  10,  10,  10,  10,  10,  11,  11,  11,
  11,  12,  12,  12,  12,  13,  13,  13,  14,  14,  14,  14,  15,  15,  15,  16,
  16,  16,  17,  17,  18,  18,  18,  19,  19,  20,  20,  21,  21,  21,  22,  22,
  23,  23,  24,  24,  25,  25,  26,  27,  27,  28,  28,  29,  30,  30,  31,  32,
  32,  33,  34,  35,  35,  36,  37,  38,  39,  39,  40,  41,  42,  43,  44,  45,
  46,  47,  48,  49,  50,  51,  52,  53,  55,  56,  57,  58,  59,  61,  62,  63,
  65,  66,  68,  69,  71,  72,  74,  76,  77,  79,  81,  82,  84,  86,  88,  90,
  92,  94,  96,  98, 100, 102, 105, 107, 109, 112, 114, 117, 119, 122, 124, 127,
 130, 133, 136, 139, 142, 145, 148, 151, 155, 158, 162, 165, 169, 172, 176, 180,
 184, 188, 192, 196, 201, 205, 210, 214, 219, 224, 229, 234, 239, 244, 250, 255
};
*/

void rgb_to_hsv(fade_rgb_t *rgb, HSV *hsv){
    int32_t min, max, delta, hue =0;
    int32_t R = rgb->r.cur>>8;
    int32_t G = rgb->g.cur>>8;
    int32_t B = rgb->b.cur>>8;

    // find min
    min = (R < G ? R : G);
    min = (min < B ? min : B);
    // find max
    max = (R > G ? R : G);
    max = (max > B ? max : B);
    // calculate delta
    delta = max - min;

    // calculate HUE
    if(R == max)
        hue = ((G - B)*60/delta);

    else if(G == max)
        hue = (120 + ((B - R)*60/delta));

    else if(B == max)
        hue = (240 + ((R - G)*60/delta));

    if(hue < 0)
        hue += 360;
    hsv->h = hue*255/360;

    // calculate Saturation
    hsv->s = 0;
    if( max != 0 )
        hsv->s = ((delta*255) / max);
    // check max saturation
    if( hsv->s > fade_default_hsv.s )
        hsv->s = fade_default_hsv.s;

    // set Value
    hsv->v = max * fade_default_hsv.v / 255;
    // hsv->v = quadratic_curve[hsv->v];
    // hsv->v = exp_curve[hsv->v];
}

void fade_color_scan(fade_color_t *c){
    if(fade_timer < fade_time ){
        // do step
        if(c->cur < c->end){
            if(c->cur >= c->end-c->step){
                c->cur = c->end;
            }else{
                c->cur += c->step;
            }
        }else{
            // cur > end
            if(c->cur <= c->end+c->step){
                c->cur = c->end;
            }else{
                c->cur -= c->step;
            }
        }
    }else{
        c->cur = c->end;
    }
}

void fade_rgb_scan(void){
    if(!fade_is_running){
        fade_time = rgb_matrix_get_speed();
        fade_default_hsv.h = rgb_matrix_get_hue();
        fade_default_hsv.s = rgb_matrix_get_sat();
        fade_default_hsv.v = rgb_matrix_get_val();
        return;
    }
    //fade_rgb_scan();

    // increment fade_timer
    if(fade_timer < fade_time)
        fade_timer++;

    // fade current to end
    fade_color_scan(&fade_rgb.r);
    fade_color_scan(&fade_rgb.g);
    fade_color_scan(&fade_rgb.b);

    // set hsv
    rgb_to_hsv(&fade_rgb, &fade_hsv);
    #ifdef RGB_MATRIX_ENABLE
        // rgb_matrix_config_hsv.hsv.h = fade_hsv.h;
        // rgb_matrix_config_hsv.hsv.s = fade_hsv.s;
        // rgb_matrix_config_hsv.hsv.v = fade_hsv.v;
        rgb_matrix_sethsv_noeeprom(fade_hsv.h, fade_hsv.s, fade_hsv.v);
    #endif
    #ifdef RGBLIGHT_ENABLE
        rgblight_sethsv_noeeprom(fade_hsv.h, fade_hsv.s, fade_hsv.v);
    #endif
    if(fade_timer >= fade_time ){
        fade_is_running = 0x00;
    }
}


void fade_rgb_scan_advanced(void){

    if(!fade_is_running){
        fade_time = rgb_matrix_get_speed();
        fade_default_hsv.h = rgb_matrix_get_hue();
        fade_default_hsv.s = rgb_matrix_get_sat();
        fade_default_hsv.v = rgb_matrix_get_val();
        return;
    }

    // increment fade_timer
    if(fade_timer < fade_time)
        fade_timer++;

    fade_rgb_t *rgb;

    for (uint8_t row = 0; row < MATRIX_ROWS; ++row) {
        for (uint8_t col = 0; col < MATRIX_COLS; ++col) {
            uint8_t index = g_led_config.matrix_co[row][col];

            if (index >= 0 && index < RGB_MATRIX_LED_COUNT && index != NO_LED){
                rgb = &fade_rgb_leds[index];

                // fade current to end
                fade_color_scan(&rgb->r);
                fade_color_scan(&rgb->g);
                fade_color_scan(&rgb->b);

                // set hsv
                rgb_to_hsv(rgb, &fade_hsv_leds[index]);

                fade_set_color(
                    index,
                    rgb->r.cur>>8,
                    rgb->g.cur>>8,
                    rgb->b.cur>>8
                );
            }
        }
    }

    if(fade_timer >= fade_time){
        fade_is_running = 0x00;
    }
}


#define FADE_CALC_STEP(cur, end, ft) (((cur>end ? cur-end : end-cur)/ft)+1)

void fade_goto(uint8_t end_r, uint8_t end_g, uint8_t end_b){

    if( (fade_rgb.r.end>>8 == end_r) &&
        (fade_rgb.g.end>>8 == end_g) &&
        (fade_rgb.b.end>>8 == end_b) ) return;

    fade_rgb.r.end = end_r<<8;
    fade_rgb.g.end = end_g<<8;
    fade_rgb.b.end = end_b<<8;

    fade_rgb.r.step = FADE_CALC_STEP(fade_rgb.r.cur, fade_rgb.r.end, fade_time);
    fade_rgb.g.step = FADE_CALC_STEP(fade_rgb.g.cur, fade_rgb.g.end, fade_time);
    fade_rgb.b.step = FADE_CALC_STEP(fade_rgb.b.cur, fade_rgb.b.end, fade_time);

    fade_timer = 0;
    fade_is_running = 0x01;
}


void fade_goto_advanced(uint8_t layer, uint8_t end_r, uint8_t end_g, uint8_t end_b){

    fade_rgb_t *rgb;

    for (uint8_t row = 0; row < MATRIX_ROWS; ++row) {
        for (uint8_t col = 0; col < MATRIX_COLS; ++col) {
            uint8_t index = g_led_config.matrix_co[row][col];
            rgb = &fade_rgb_leds[index];

            if (index >= 0 && index < RGB_MATRIX_LED_COUNT && index != NO_LED &&
                keymap_key_to_keycode(layer, (keypos_t){col,row}) > KC_TRNS) {
                if( (rgb->r.end>>8 == end_r) &&
                    (rgb->g.end>>8 == end_g) &&
                    (rgb->b.end>>8 == end_b) )
                    continue;
                rgb->r.end = end_r<<8;
                rgb->g.end = end_g<<8;
                rgb->b.end = end_b<<8;
            }else{
                if( (rgb->r.end == 0) &&
                    (rgb->g.end == 0) &&
                    (rgb->b.end == 0) )
                    continue;
                rgb->r.end = 0;
                rgb->g.end = 0;
                rgb->b.end = 0;
            }

            rgb->r.step = FADE_CALC_STEP(rgb->r.cur, rgb->r.end, fade_time);
            rgb->g.step = FADE_CALC_STEP(rgb->g.cur, rgb->g.end, fade_time);
            rgb->b.step = FADE_CALC_STEP(rgb->b.cur, rgb->b.end, fade_time);
        }
    }

    fade_timer = 0;
    fade_is_running = 0x01;
}


void fade_init(void){
    #ifdef RGB_MATRIX_ENABLE
        rgb_matrix_config.hsv = (HSV){0,0,0};
        // if(rgb_matrix_get_mode()==RGB_MATRIX_SOLID_COLOR){
        //     rgb_matrix_sethsv_noeeprom(0, 0, 0);
        // }
    #endif
    #ifdef RGBLIGHT_ENABLE
        rgblight_sethsv_noeeprom(0, 0, 0); // set leds off
    #endif
}

void fade_instant_off_goto_rgb_end(void){
    RGB end = {
        r: fade_rgb.r.cur,
        g: fade_rgb.g.cur,
        b: fade_rgb.b.cur
    };
    fade_rgb.r.cur=0;
    fade_rgb.g.cur=0;
    fade_rgb.b.cur=0;
    fade_goto(end.r, end.g, end.b);
}
