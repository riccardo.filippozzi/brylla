#include QMK_KEYBOARD_H

#include "fade.h"

// Custom colors
#define RGB_LIME_GREEN      50, 205, 50

// Layers colors
#define RGB_LAYER_NUMERIC   RGB_RED
#define RGB_LAYER_NAV       RGB_BLUE
#define RGB_LAYER_SYMBOLS   RGB_GREEN
#define RGB_LAYER_FUNCTIONS RGB_YELLOW
#define RGB_LAYER_ADJUST    RGB_PURPLE
#define RGB_LAYER_MOUSE     RGB_CYAN
#define RGB_LAYER_DEFAULT   RGB_LIME_GREEN

#define RGB_INDICATOR       RGB_WHITE


// One Shot Mod
#define OSM_LSFT    OSM(MOD_LSFT)
#define OSM_RSFT    OSM(MOD_RSFT)
#define OSM_LCTL    OSM(MOD_LCTL)
#define OSM_RCTL    OSM(MOD_RCTL)
#define OSM_LALT    OSM(MOD_LALT)
#define OSM_RALT    OSM(MOD_RALT)

// Macro
#define LALT_F4     LALT(KC_F4)

// QWERTY Home Row Left
#define HR_GUI_A LGUI_T(KC_A)
#define HR_ALT_S LALT_T(KC_S)
#define HR_CTL_D LCTL_T(KC_D)
#define HR_SFT_F LSFT_T(KC_F)

// QWERTY Home Row Right
#define HR_GUI_SCLN RGUI_T(KC_SCLN)
#define HR_ALT_L    LALT_T(KC_L)
#define HR_CTL_K    RCTL_T(KC_K)
#define HR_SFT_J    RSFT_T(KC_J)

// DVORAK Home Row Left
// #define HR_GUI_A LGUI_T(KC_A)
#define HR_ALT_O LALT_T(KC_O)
#define HR_CTL_E LCTL_T(KC_E)
#define HR_SFT_U LSFT_T(KC_U)

// DVORAK Home Row Right
#define HR_GUI_S RGUI_T(KC_S)
#define HR_ALT_N LALT_T(KC_N)
#define HR_CTL_T RCTL_T(KC_T)
#define HR_SFT_H RSFT_T(KC_H)

// COLEMAK Home Row Left
// #define HR_GUI_A LGUI_T(KC_A)
#define HR_ALT_R LALT_T(KC_R)
#define HR_CTL_S LCTL_T(KC_S)
#define HR_SFT_T LSFT_T(KC_T)

// COLEMAK Home Row Right
#define HR_GUI_O RGUI_T(KC_O)
#define HR_ALT_I LALT_T(KC_I)
// #define HR_CTL_E RCTL_T(KC_E)
#define HR_SFT_N RSFT_T(KC_N)

// NUMERIC Home Row Left
#define HR_GUI_PAST LGUI_T(KC_PAST)
#define HR_ALT_PSLS LALT_T(KC_PSLS)
#define HR_CTL_PPLS LCTL_T(KC_PPLS)
#define HR_SFT_PMNS LSFT_T(KC_PMNS)

// Shortcuts
#define SH_CTL_Z LCTL(KC_Z)
#define SH_CTL_X LCTL(KC_X)
#define SH_CTL_C LCTL(KC_C)
#define SH_CTL_V LCTL(KC_V)
#define SH_CTL_Y LCTL(KC_Y)
#define SH_ALT_TAB LALT(KC_TAB)
#define SH_LCA_TAB LCA(KC_TAB)

// Layers
#define MO_ADJUST   MO(_ADJUST)
#define MO_MOUSE    MO(_MOUSE)
#define MO_NAV      MO(_NAV)
#define MO_SYMBOLS  MO(_SYMBOLS)
#define LT_ADJ_CAPS LT(_ADJUST,KC_CAPS)
#define LT_NUM_ENT  LT(_NUMERIC,KC_ENT)
#define LT_FUN_ESC  LT(_FUNCTIONS,KC_ESC)
#define LT_FUN_APP  LT(_FUNCTIONS,KC_APP)
#define LT_SYM_ENT  LT(_SYMBOLS,KC_ENTER)
#define DF_QWERTY   DF(_QWERTY)
#define DF_DVORAK   DF(_DVORAK)
#define DF_CLMAK    DF(_COLEMAK)
#define DF_CLMAK_DH DF(_COLEMAK_DH)
#define DF_GAM_WASD DF(_GAMING_WASD)

// to improve readability
#define ____TRN____ KC_TRNS
#define __PRESSED__ KC_TRNS
#define ___________ KC_NO

#define TD_L_LAYERS TD(_TD_L_LAYERS)
#define TD_R_LAYERS TD(_TD_R_LAYERS)
#define TD_LBRS     TD(_TD_LBRS)
#define TD_RBRS     TD(_TD_RBRS)
#define TD_LYCL_ENT TD(_TD_LYCL_ENT)

// tap dance layers buttons flags
#define TD_HOLD_L_LAYERS 0x10
#define TD_STAP_L_LAYERS 0x20
#define TD_HOLD_R_LAYERS 0x01
#define TD_STAP_R_LAYERS 0x02
#define TD_HOLD_L_STAP_R_LAYERS (TD_HOLD_L_LAYERS | TD_STAP_R_LAYERS)
#define TD_HOLD_R_STAP_L_LAYERS (TD_HOLD_R_LAYERS | TD_STAP_L_LAYERS)

// layers name
enum  {
    _QWERTY,
    _DVORAK,
    _COLEMAK,
    _COLEMAK_DH,
    _GAMING_WASD,
    _NUMERIC,
    _NAV,
    _SYMBOLS,
    _FUNCTIONS,
    _ADJUST,
    _MOUSE
};

// tap dance buttons
enum {
    _TD_L_LAYERS,
    _TD_R_LAYERS,
    _TD_LBRS,
    _TD_RBRS,
};

typedef enum {
    TD_NONE,
    TD_UNKNOWN,
    TD_SINGLE_TAP,
    TD_SINGLE_HOLD,
    TD_DOUBLE_TAP,
    TD_DOUBLE_HOLD,
    TD_DOUBLE_SINGLE_TAP,
    TD_TRIPLE_TAP,
    TD_TRIPLE_SINGLE_TAP,
    TD_TRIPLE_HOLD
} td_state_t;

static td_state_t td_state_l_layers;
static td_state_t td_state_r_layers;
static td_state_t td_state_lbrs;
static td_state_t td_state_rbrs;

#define LAYER_LOCK_MASK  0x03
#define LAYER_LOCK_LEFT  0x01
#define LAYER_LOCK_RIGHT 0x02

static uint8_t layer_lock = 0x00; // used to have smart layers movements

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
	[_QWERTY] = LAYOUT_split_4x6_5(
    // ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗  ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗
        KC_ESC,      ____TRN____, ____TRN____, ____TRN____, ____TRN____, QK_BOOT,        QK_BOOT,     KC_CAPS,     KC_NUM,      KC_SCRL,     ____TRN____, KC_ESC,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        KC_TAB,      KC_Q,        KC_W,        KC_E,        KC_R,        KC_T,           KC_Y,        KC_U,        KC_I,        KC_O,        KC_P,        KC_ESC,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        KC_GRV,      HR_GUI_A,    HR_ALT_S,    HR_CTL_D,    HR_SFT_F,    KC_G,           KC_H,        HR_SFT_J,    HR_CTL_K,    HR_ALT_L,    HR_GUI_SCLN, KC_QUOT,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        TD_LBRS,     KC_Z,        KC_X,        KC_C,        KC_V,        KC_B,           KC_N,        KC_M,        KC_COMM,     KC_DOT,      KC_SLSH,     TD_RBRS,
    // ╚════════════╩════════════╩════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╩════════════╩════════════╝
                                               KC_DEL,      KC_SPC,      TD_L_LAYERS,    TD_R_LAYERS, KC_BSPC,     KC_ENT,
    //                                        ╚════════════╬════════════╩════════════╣  ╠════════════╩════════════╩════════════╝
                                                            KC_LBRC,     KC_LCBR,        KC_RCBR,     KC_RBRC
    //                                                     ╚════════════╩════════════╝  ╚════════════╩════════════╝
    ),
	[_NUMERIC] = LAYOUT_split_4x6_5(
    // ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗  ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗
        ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        KC_CIRC,     KC_TILD,     KC_EXLM,     KC_PIPE,     KC_AMPR,     ___________,    KC_NUM,      KC_P7,       KC_P8,       KC_P9,       KC_PCMM,     KC_ESC,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        KC_PERC,     HR_GUI_PAST, HR_ALT_PSLS, HR_CTL_PPLS, HR_SFT_PMNS, ___________,    KC_P0,       KC_P4,       KC_P5,       KC_P6,       KC_PDOT,     KC_EQL,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        TD_LBRS,     ___________, ___________, ___________, ___________, ___________,    KC_P0,       KC_P1,       KC_P2,       KC_P3,       KC_PENT,     TD_RBRS,
    // ╚════════════╩════════════╩════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╩════════════╩════════════╝
                                               ____TRN____, ____TRN____, TD_L_LAYERS,    TD_R_LAYERS, ____TRN____, ____TRN____,
    //                                        ╚════════════╬════════════╩════════════╣  ╠════════════╩════════════╩════════════╝
                                                            ____TRN____, ____TRN____,    ____TRN____, ____TRN____
    //                                                     ╚════════════╩════════════╝  ╚════════════╩════════════╝
    ),
	[_NAV] = LAYOUT_split_4x6_5(
    // ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗  ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗
        ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        ____TRN____, SH_CTL_Y,    ___________, SH_ALT_TAB,  SH_LCA_TAB,  ___________,    KC_PGUP,     KC_HOME,     KC_UP,       KC_END,      ___________, KC_ESC,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        ___________, KC_LGUI,     KC_LALT,     KC_LCTL,     KC_LSFT,     ___________,    KC_PGDN,     KC_LEFT,     KC_DOWN,     KC_RGHT,     KC_PSCR,     KC_INS,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        ___________, SH_CTL_Z,    SH_CTL_X,    SH_CTL_C,    SH_CTL_V,    ___________,    ___________, KC_PGUP,     ___________, KC_PGDN,     ___________, ___________,
    // ╚════════════╩════════════╩════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╩════════════╩════════════╝
                                               ____TRN____, ____TRN____, TD_L_LAYERS,    TD_R_LAYERS, ____TRN____, ____TRN____,
    //                                        ╚════════════╬════════════╩════════════╣  ╠════════════╩════════════╩════════════╝
                                                            ____TRN____, ____TRN____,    ____TRN____, ____TRN____
    //                                                     ╚════════════╩════════════╝  ╚════════════╩════════════╝
    ),
	[_SYMBOLS] = LAYOUT_split_4x6_5(
    // ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗  ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗
        ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        KC_CIRC,     KC_TILD,     KC_EXLM,     KC_PIPE,     KC_AMPR,     KC_AT,          KC_CAPS,     KC_7,        KC_8,        KC_9,        KC_COMM,     KC_ESC,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        KC_PERC,     KC_ASTR,     KC_PSLS,     KC_PPLS,     KC_PMNS,     ___________,    KC_0,        KC_4,        KC_5,        KC_6,        KC_PDOT,     KC_EQL,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        TD_LBRS,     KC_DLR,      KC_BSLS,     KC_HASH,     KC_UNDS,     ___________,    KC_0,        KC_1,        KC_2,        KC_3,        KC_PDOT,     TD_RBRS,
    // ╚════════════╩════════════╩════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╩════════════╩════════════╝
                                               ____TRN____, ____TRN____, TD_L_LAYERS,    TD_R_LAYERS, ____TRN____, ____TRN____,
    //                                        ╚════════════╬════════════╩════════════╣  ╠════════════╩════════════╩════════════╝
                                                            ____TRN____, ____TRN____,    ____TRN____, ____TRN____
    //                                                     ╚════════════╩════════════╝  ╚════════════╩════════════╝
    ),
	[_FUNCTIONS] = LAYOUT_split_4x6_5(
    // ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗  ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗
        ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        ___________, KC_F9,       KC_F10,      KC_F11,      KC_F12,      ___________,    KC_VOLU,     KC_MRWD,     KC_PAUS,     KC_MFFD,     ___________, ___________,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        ___________, KC_F5,       KC_F6,       KC_F7,       KC_F8,       ___________,    KC_VOLD,     KC_RSFT,     KC_RCTL,     KC_LALT,     KC_RGUI,     ___________,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        ___________, KC_F1,       KC_F2,       KC_F3,       KC_F4,       ___________,    KC_MUTE,     KC_MPRV,     KC_MPLY,     KC_MNXT,     ___________, ___________,
    // ╚════════════╩════════════╩════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╩════════════╩════════════╝
                                               ____TRN____, ____TRN____, TD_L_LAYERS,    TD_R_LAYERS, ____TRN____, ____TRN____,
    //                                        ╚════════════╬════════════╩════════════╣  ╠════════════╩════════════╩════════════╝
                                                            ____TRN____, ____TRN____,    ____TRN____, ____TRN____
    //                                                     ╚════════════╩════════════╝  ╚════════════╩════════════╝
    ),
	[_ADJUST] = LAYOUT_split_4x6_5(
    // ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗  ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗
        ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        DF_QWERTY,   ___________, ___________, ___________, ___________, ___________,    ___________, ___________, ___________, ___________, ___________, ___________,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        DF_DVORAK,   ___________, ___________, ___________, ___________, DF_GAM_WASD,    RGB_TOG,     RGB_HUI,     RGB_SAI,     RGB_VAI,     RGB_MOD,     RGB_SPI,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        DF_CLMAK,    DF_CLMAK_DH, ___________, ___________, ___________, ___________,    ___________, RGB_HUD,     RGB_SAD,     RGB_VAD,     RGB_RMOD,    RGB_SPD,
    // ╚════════════╩════════════╩════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╩════════════╩════════════╝
                                               ____TRN____, ____TRN____, TD_L_LAYERS,    TD_R_LAYERS, ____TRN____, ____TRN____,
    //                                        ╚════════════╬════════════╩════════════╣  ╠════════════╩════════════╩════════════╝
                                                            ____TRN____, ____TRN____,    ____TRN____, ____TRN____
    //                                                     ╚════════════╩════════════╝  ╚════════════╩════════════╝
    ),
	[_MOUSE] = LAYOUT_split_4x6_5(
    // ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗  ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗
        ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        ___________, KC_BTN3,     KC_BTN2,     KC_WH_U,     KC_BTN1,     ___________,    KC_SCRL,     ___________, KC_MS_U,     ___________, ___________, ___________,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        ___________, KC_BTN4,     KC_WH_L,     KC_WH_D,     KC_WH_R,     ___________,    ___________, KC_MS_L,     KC_MS_D,     KC_MS_R,     ___________, ___________,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        ___________, KC_BTN5,     SH_CTL_X,    SH_CTL_C,    SH_CTL_V,    ___________,    ___________, KC_ACL0,     KC_ACL1,     KC_ACL2,     ___________, ___________,
    // ╚════════════╩════════════╩════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╩════════════╩════════════╝
                                               ____TRN____, ____TRN____, TD_L_LAYERS,    TD_R_LAYERS, ____TRN____, ____TRN____,
    //                                        ╚════════════╬════════════╩════════════╣  ╠════════════╩════════════╩════════════╝
                                                            ____TRN____, ____TRN____,    ____TRN____, ____TRN____
    //                                                     ╚════════════╩════════════╝  ╚════════════╩════════════╝
    ),
	[_GAMING_WASD] = LAYOUT_split_4x6_5(
    // ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗  ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗
        ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        KC_TAB,      KC_Q,        KC_W,        KC_E,        KC_R,        KC_T,           KC_Y,        KC_U,        KC_I,        KC_O,        KC_P,        KC_ESC,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        KC_GRV,      KC_A,        KC_S,        KC_D,        KC_F,        KC_G,           KC_H,        KC_J,        KC_K,        KC_L,        KC_SCLN,     KC_QUOT,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        TD_LBRS,     KC_Z,        KC_X,        KC_C,        KC_V,        KC_B,           KC_N,        KC_M,        KC_COMM,     KC_DOT,      KC_SLSH,     TD_RBRS,
    // ╚════════════╩════════════╩════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╩════════════╩════════════╝
                                               KC_DEL,      KC_SPC,      TD_L_LAYERS,    TD_R_LAYERS, KC_ENT,      KC_BSPC,
    //                                        ╚════════════╬════════════╩════════════╣  ╠════════════╩════════════╩════════════╝
                                                            KC_LBRC,     KC_LCBR,        KC_RCBR,     KC_RBRC
    //                                                     ╚════════════╩════════════╝  ╚════════════╩════════════╝
    ),
	[_DVORAK] = LAYOUT_split_4x6_5(

    // ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗  ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗
        ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        KC_TAB,      KC_QUOT,     KC_COMM,     KC_DOT,      KC_P,        KC_Y,           KC_F,        KC_G,        KC_C,        KC_R,        KC_L,        KC_ESC,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        KC_GRV,      HR_GUI_A,    HR_ALT_O,    HR_CTL_E,    HR_SFT_U,    KC_I,           KC_D,        HR_SFT_H,    HR_CTL_T,    HR_ALT_N,    HR_GUI_S,    KC_SLSH,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        TD_LBRS,     KC_SCLN,     KC_Q,        KC_J,        KC_K,        KC_X,           KC_B,        KC_M,        KC_W,        KC_V,        KC_Z,        TD_RBRS,
    // ╚════════════╩════════════╩════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╩════════════╩════════════╝
                                               KC_DEL,      KC_SPC,      TD_L_LAYERS,    TD_R_LAYERS, KC_ENT,      KC_BSPC,
    //                                        ╚════════════╬════════════╩════════════╣  ╠════════════╩════════════╩════════════╝
                                                            KC_LBRC,     KC_LCBR,        KC_RCBR,     KC_RBRC
    //                                                     ╚════════════╩════════════╝  ╚════════════╩════════════╝
    ),
	[_COLEMAK] = LAYOUT_split_4x6_5(
    // ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗  ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗
        ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        KC_TAB,      KC_Q,        KC_W,        KC_F,        KC_P,        KC_G,           KC_J,        KC_L,        KC_U,        KC_Y,        KC_SCLN,     KC_ESC,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        KC_GRV,      HR_GUI_A,    HR_ALT_R,    HR_CTL_S,    HR_SFT_T,    KC_D,           KC_H,        HR_SFT_N,    HR_CTL_E,    HR_ALT_I,    HR_GUI_O,    KC_QUOT,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        TD_LBRS,     KC_Z,        KC_X,        KC_C,        KC_V,        KC_B,           KC_K,        KC_M,        KC_COMM,     KC_DOT,      KC_SLSH,     TD_RBRS,
    // ╚════════════╩════════════╩════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╩════════════╩════════════╝
                                               KC_DEL,      KC_SPC,      TD_L_LAYERS,    TD_R_LAYERS, KC_ENT,      KC_BSPC,
    //                                        ╚════════════╬════════════╩════════════╣  ╠════════════╩════════════╩════════════╝
                                                            KC_LBRC,     KC_LCBR,        KC_RCBR,     KC_RBRC
    //                                                     ╚════════════╩════════════╝  ╚════════════╩════════════╝
    ),
	[_COLEMAK_DH] = LAYOUT_split_4x6_5(
    // ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗  ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗
        ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        KC_TAB,      KC_Q,        KC_W,        KC_F,        KC_P,        KC_B,           KC_J,        KC_L,        KC_U,        KC_Y,        KC_SCLN,     KC_ESC,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        KC_GRV,      HR_GUI_A,    HR_ALT_R,    HR_CTL_S,    HR_SFT_T,    KC_G,           KC_M,        HR_SFT_N,    HR_CTL_E,    HR_ALT_I,    HR_GUI_O,    KC_QUOT,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
        TD_LBRS,     KC_X,        KC_C,        KC_D,        KC_V,        KC_Z,           KC_K,        KC_H,        KC_COMM,     KC_DOT,      KC_SLSH,     TD_RBRS,
    // ╚════════════╩════════════╩════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╩════════════╩════════════╝
                                               KC_DEL,      KC_SPC,      TD_L_LAYERS,    TD_R_LAYERS, KC_ENT,      KC_BSPC,
    //                                        ╚════════════╬════════════╩════════════╣  ╠════════════╩════════════╩════════════╝
                                                            KC_LBRC,     KC_LCBR,        KC_RCBR,     KC_RBRC
    //                                                     ╚════════════╩════════════╝  ╚════════════╩════════════╝
    ),
};

    // ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗  ╔════════════╦════════════╦════════════╦════════════╦════════════╦════════════╗
    //  ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
    //  ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
    //  ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,
    // ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╬════════════╬════════════╣
    //  ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____, ____TRN____,
    // ╚════════════╩════════════╩════════════╬════════════╬════════════╬════════════╣  ╠════════════╬════════════╬════════════╬════════════╩════════════╩════════════╝
    //                                         ____TRN____, ____TRN____, ____TRN____,    ____TRN____, ____TRN____, ____TRN____,
    //                                        ╚════════════╬════════════╩════════════╣  ╠════════════╩════════════╩════════════╝
    //                                                      ____TRN____, ____TRN____,    ____TRN____, ____TRN____
    //                                                     ╚════════════╩════════════╝  ╚════════════╩════════════╝


// Start Tap Dance

td_state_t cur_dance(tap_dance_state_t *state){
    if (state->count == 1) {
        if (state->interrupted || !state->pressed) return TD_SINGLE_TAP;
        else return TD_SINGLE_HOLD;

    } else if (state->count == 2) {
        if (state->interrupted) return TD_DOUBLE_SINGLE_TAP;
        else if (state->pressed) return TD_DOUBLE_HOLD;
        else return TD_DOUBLE_TAP;

    }else if (state->count == 3) {
        if (state->interrupted) return TD_TRIPLE_SINGLE_TAP;
        else if (state->pressed) return TD_TRIPLE_HOLD;
        else return TD_TRIPLE_TAP;
    } else return TD_UNKNOWN;
}


uint8_t td_l_layers_actions(uint8_t state){
    switch(td_state_l_layers){
        default: layer_clear(); break;

        case TD_SINGLE_HOLD:
            layer_move(_NAV);
            return 1;

        case TD_DOUBLE_HOLD:
            layer_move(_MOUSE);
            return 2;

        case TD_TRIPLE_HOLD:
            layer_move(_ADJUST);
            return 3;
    }
    return 0;
}

uint8_t td_r_layers_actions(uint8_t state){
    switch(state){
        default: layer_clear(); break;

        case TD_SINGLE_HOLD:
            layer_move(_SYMBOLS);
            return 1;

        case TD_DOUBLE_HOLD:
            layer_move(_FUNCTIONS);
            return 2;

        case TD_TRIPLE_HOLD:
            layer_move(_NUMERIC);
            return 3;
    }
    return 0;
}

void td_l_layers_each(tap_dance_state_t *state, void *user_data) {
    td_state_l_layers = cur_dance(state);
    if(td_l_layers_actions(td_state_l_layers)>1 && td_state_r_layers!=TD_NONE && td_state_r_layers!=TD_UNKNOWN){
        layer_lock |= LAYER_LOCK_RIGHT<<4;
    }
}

void td_r_layers_each(tap_dance_state_t *state, void *user_data) {
    td_state_r_layers = cur_dance(state);
    if(td_r_layers_actions(td_state_r_layers)>1 && td_state_l_layers!=TD_NONE && td_state_l_layers!=TD_UNKNOWN){
        layer_lock |= LAYER_LOCK_LEFT<<4;
    }
}

void td_l_layers_reset(tap_dance_state_t *state, void *user_data) {
    if(td_state_r_layers!=TD_NONE && td_state_r_layers!=TD_UNKNOWN){
        td_r_layers_actions(td_state_r_layers);
        if((layer_lock>>4)&LAYER_LOCK_RIGHT){
            layer_lock |= LAYER_LOCK_RIGHT;
        }
    }else if(!(layer_lock&LAYER_LOCK_LEFT)){
        switch(td_state_l_layers){
            case TD_SINGLE_HOLD:
                if(get_highest_layer(layer_state)==_NAV){
                    layer_clear();
                }
                break;
            case TD_DOUBLE_HOLD:
                if(get_highest_layer(layer_state)==_MOUSE){
                    layer_clear();
                }
                break;
            case TD_TRIPLE_HOLD:
                if(get_highest_layer(layer_state)==_ADJUST){
                    layer_clear();
                }
                break;
            default: break;
        }
    }
    layer_lock &= ~((LAYER_LOCK_LEFT<<4)|LAYER_LOCK_LEFT);
    td_state_l_layers = TD_NONE;
}

void td_r_layers_reset(tap_dance_state_t *state, void *user_data) {
    if(td_state_l_layers!=TD_NONE && td_state_l_layers!=TD_UNKNOWN){
        td_l_layers_actions(td_state_l_layers);
        if((layer_lock>>4)&LAYER_LOCK_LEFT){
            layer_lock |= LAYER_LOCK_LEFT;
        }
    }else if(!(layer_lock&LAYER_LOCK_RIGHT)){
        switch(td_state_r_layers){
            case TD_SINGLE_HOLD:
                if(get_highest_layer(layer_state)==_SYMBOLS){
                    layer_clear();
                }
                break;
            case TD_DOUBLE_HOLD:
                if(get_highest_layer(layer_state)==_FUNCTIONS){
                    layer_clear();
                }
                break;
            case TD_TRIPLE_HOLD:
                if(get_highest_layer(layer_state)==_NUMERIC){
                    layer_clear();
                }
                break;
            default: break;
        }
    }
    layer_lock &= ~((LAYER_LOCK_RIGHT<<4)|LAYER_LOCK_RIGHT);
    td_state_r_layers = TD_NONE;
}

void td_lbrs_finished(tap_dance_state_t *state, void *user_data){
    td_state_lbrs = cur_dance(state);
    switch(td_state_lbrs){
        case TD_SINGLE_TAP:
        case TD_SINGLE_HOLD:
            if(get_mods() & MOD_MASK_SHIFT){
                register_code16(KC_LCBR); // {
            }else{
                register_code16(KC_LPRN); // (
            }
            break;
        case TD_DOUBLE_TAP:
        case TD_DOUBLE_SINGLE_TAP:
        case TD_DOUBLE_HOLD:
            register_code16(KC_LBRC); // [
            break;
        case TD_TRIPLE_TAP:
        case TD_TRIPLE_SINGLE_TAP:
        case TD_TRIPLE_HOLD:
            register_code16(KC_LCBR); // {
            break;
        default: break;
    }
}

void td_lbrs_reset(tap_dance_state_t *state, void *user_data){
    switch(td_state_lbrs){
        case TD_SINGLE_TAP:
        case TD_SINGLE_HOLD:
            unregister_code16(KC_LCBR); // {
            unregister_code16(KC_LPRN); // (
            break;
        case TD_DOUBLE_TAP:
        case TD_DOUBLE_SINGLE_TAP:
        case TD_DOUBLE_HOLD:
            unregister_code16(KC_LBRC); // [
            break;
        case TD_TRIPLE_TAP:
        case TD_TRIPLE_SINGLE_TAP:
        case TD_TRIPLE_HOLD:
            unregister_code16(KC_LCBR); // {
            break;
        default: break;
    }
    td_state_lbrs = TD_NONE;
}

void td_rbrs_finished(tap_dance_state_t *state, void *user_data){
    td_state_rbrs = cur_dance(state);
    switch(td_state_rbrs){
        case TD_SINGLE_TAP:
        case TD_SINGLE_HOLD:
            if(get_mods() & MOD_MASK_SHIFT){
                register_code16(KC_RCBR); // }
            }else{
                register_code16(KC_RPRN); // )
            }
            break;
        case TD_DOUBLE_TAP:
        case TD_DOUBLE_SINGLE_TAP:
        case TD_DOUBLE_HOLD:
            register_code16(KC_RBRC); // ]
            break;
        case TD_TRIPLE_TAP:
        case TD_TRIPLE_SINGLE_TAP:
        case TD_TRIPLE_HOLD:
            register_code16(KC_RCBR); // }
            break;
        default: break;
    }
}

void td_rbrs_reset(tap_dance_state_t *state, void *user_data){
    switch(td_state_rbrs){
        case TD_SINGLE_TAP:
        case TD_SINGLE_HOLD:
            unregister_code16(KC_RCBR); // }
            unregister_code16(KC_RPRN); // )
            break;
        case TD_DOUBLE_TAP:
        case TD_DOUBLE_SINGLE_TAP:
        case TD_DOUBLE_HOLD:
            unregister_code16(KC_RBRC); // ]
            break;
        case TD_TRIPLE_TAP:
        case TD_TRIPLE_SINGLE_TAP:
        case TD_TRIPLE_HOLD:
            unregister_code16(KC_RCBR); // }
            break;
        default: break;
    }
    td_state_rbrs = TD_NONE;
}

tap_dance_action_t tap_dance_actions[] = {
    [_TD_L_LAYERS] = ACTION_TAP_DANCE_FN_ADVANCED(td_l_layers_each, NULL, td_l_layers_reset),
    [_TD_R_LAYERS] = ACTION_TAP_DANCE_FN_ADVANCED(td_r_layers_each, NULL, td_r_layers_reset),
    [_TD_LBRS] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, td_lbrs_finished, td_lbrs_reset),
    [_TD_RBRS] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, td_rbrs_finished, td_rbrs_reset),
};

// End Tap Dance

void keyboard_pre_init_user(){
    rgb_matrix_mode(RGB_MATRIX_CUSTOM_fade_effect);
}

bool rgb_matrix_indicators_advanced_user(uint8_t led_min, uint8_t led_max) {

    // fade_rgb_scan_advanced(led_min, led_max);

    // Host Keyboard LED Status
    led_t led_usb_state = host_keyboard_led_state();

    // num lock
    if(led_usb_state.num_lock){
        fade_set_color(44, RGB_INDICATOR);
        // fade_set_color(15, RGB_INDICATOR);
    }

    // caps lock
    if(led_usb_state.caps_lock){
        fade_set_color(45, RGB_INDICATOR);
        // fade_set_color(16, RGB_INDICATOR);
    }

    // scroll lock
    if(led_usb_state.scroll_lock){
        fade_set_color(37, RGB_INDICATOR);
        // fade_set_color(8, RGB_INDICATOR);
    }

    // Keyboard Layer Status
    bool enable_layer_indicator = true;
    #ifdef RGB_MATRIX_ENABLE
        if( rgb_matrix_get_mode()==RGB_MATRIX_CUSTOM_fade_effect ||
            rgb_matrix_get_mode()==RGB_MATRIX_CUSTOM_solid_layer ||
            rgb_matrix_get_mode()==RGB_MATRIX_CUSTOM_solid_layer_fade){
            enable_layer_indicator = false;
        }
    #endif
    #ifdef RGBLIGHT_ENABLE
        enable_layer_indicator = false;
    #endif
    if(enable_layer_indicator){
        switch (get_highest_layer(layer_state|default_layer_state)) {
            case _NUMERIC:
                fade_set_color(26, RGB_LAYER_NUMERIC);
                fade_set_color(55, RGB_LAYER_NUMERIC);
                break;
            case _NAV:
                fade_set_color(26, RGB_LAYER_NAV);
                fade_set_color(55, RGB_LAYER_NAV);
                break;
            case _SYMBOLS:
                fade_set_color(26, RGB_LAYER_SYMBOLS);
                fade_set_color(55, RGB_LAYER_SYMBOLS);
                break;
            case _FUNCTIONS:
                fade_set_color(26, RGB_LAYER_FUNCTIONS);
                fade_set_color(55, RGB_LAYER_FUNCTIONS);
                break;
            case _ADJUST:
                fade_set_color(26, RGB_LAYER_ADJUST);
                fade_set_color(55, RGB_LAYER_ADJUST);
                break;
            case _MOUSE:
                fade_set_color(26, RGB_LAYER_MOUSE);
                fade_set_color(55, RGB_LAYER_MOUSE);
                break;
            default:
                fade_set_color(26, RGB_LAYER_DEFAULT);
                fade_set_color(55, RGB_LAYER_DEFAULT);
                break;
        }
    }
    return false;
}
