
#ifndef FADE_H
#define FADE_H

void fade_init(void); // call this in keyboard_pre_init_user
void fade_rgb_scan(void); // call this in matrix_scan_user
void fade_rgb_scan_advanced(void);

void fade_goto(uint8_t end_r, uint8_t end_g, uint8_t end_b);
void fade_goto_advanced(uint8_t layer, uint8_t end_r, uint8_t end_g, uint8_t end_b);
void fade_instant_off_goto_rgb_end(void);

// buttons functions
bool fade_button_fd_time(void);
bool fade_button_fd_hue(void);
bool fade_button_fd_sat(void);
bool fade_button_fd_val(void);

RGB fade_get_current_rgb(void);

void fade_set_color(uint8_t led, uint8_t r, uint8_t g, uint8_t b);

#endif
